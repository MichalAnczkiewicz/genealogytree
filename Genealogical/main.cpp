#include <iostream>
#include <windows.h>

using namespace std;

string family[16]; //array which stores family members
bool empty[16]; //array which keeps info if there's empty place in the tree
string *root; //for adding family members


void setCursorPosInConsole(int x, int y);

void displayTree();

void addMember();


int main(){
    for (int i=1; i<=15; i++){
        empty[i]=true;
    }
    root=NULL;

	do{
	    displayTree();
	    addMember();
	}while(empty[15]==true);

	displayTree();
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),12);
	setCursorPosInConsole(1,28);
	cout<<family[1]<<", you've finished making your family tree! Good job!"<<endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),15);
    return 0;
}
void setCursorPosInConsole(int x, int y){ //setting cursor on pos x,y in console
  COORD c = {x,y};
  SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),c); //sets cursor on given pos in std output - console
}

void displayTree(){ //displaying tree
	system("CLS");
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),10);
	setCursorPosInConsole(33,1);
	cout << "---------------FAMILY TREE----------------";
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),7);
	setCursorPosInConsole(1,14);
	if (empty[1]==true)
        cout<<"(me)";
    else
        cout<<family[1];
	setCursorPosInConsole(15,8);
	if (empty[2]==true)
        cout<<"(father)";
    else
        cout<<family[2];
	setCursorPosInConsole(15,20);
	if (empty[3]==true)
        cout<<"(mother)";
    else
        cout<<family[3];
	setCursorPosInConsole(45,5);
	if (empty[4]==true)
        cout<<"(grandfather1)";
    else
        cout<<family[4];
	setCursorPosInConsole(45,11);
	if (empty[5]==true)
        cout<<"(grandmother1)";
    else
        cout<<family[5];
	setCursorPosInConsole(45,17);
	if (empty[6]==true)
        cout<<"(grandfather2)";
    else
        cout<<family[6];
	setCursorPosInConsole(45,23);
	if (empty[7]==true)
        cout<<"(grandmother2)";
    else
        cout<<family[7];
	setCursorPosInConsole(90,3);
	if (empty[8]==true)
        cout<<"(great-grandfather1)";
    else
        cout<<family[8];
	setCursorPosInConsole(90,6);
	if (empty[9]==true)
        cout<<"(great-grandmother1)";
    else
        cout<<family[9];
	setCursorPosInConsole(90,9);
	if (empty[10]==true)
        cout<<"(great-grandfather2)";
    else
        cout<<family[10];
	setCursorPosInConsole(90,12);
	if (empty[11]==true)
        cout<<"(great-grandmother2)";
    else
    cout<<family[11];
	setCursorPosInConsole(90,15);
	if (empty[12]==true)
        cout<<"(great-grandfather3)";
    else
        cout<<family[12];
	setCursorPosInConsole(90,18);
	if (empty[13]==true)
        cout<<"(great-grandmother3)";
    else
        cout<<family[13];
	setCursorPosInConsole(90,21);
	if (empty[14]==true)
        cout<<"(great-grandfather4)";
    else
        cout<<family[14];
	setCursorPosInConsole(90,24);
    if (empty[15]==true)
        cout<<"(great-grandmother4)";
    else
        cout<<family[15];
	cout<<endl<<endl<<endl<<endl<<endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),3);
}

void addMember(){ //adding family member
    string member;
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),10);
	cout<<"Add family member - adding members by the branches from the left and the top: ";
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),15);
    cin>>member;
    if (empty[1]==true){ //if tree is empty
        root=&family[1]; //new member is on max on the left
        *root=member; // root points on member
        empty[1]=false; //not empty anymore -> displaying actual name of family member
    }
    else{ //tree isn't empty
    bool foundPlace=false; //flag for while loop
    int arrIndex=1; //index in arr
    while (foundPlace==false){ //until foundPlace is false
        if (empty[arrIndex]==true){ // if the array with the specified index is empty
            foundPlace=true; //changing flag for exiting while loop
            family[arrIndex]=member; //assigning a member to the array under specified index
            empty[arrIndex]=false; //this position is not empty anymore
        //moving on to next position
            }
             arrIndex++;
        }
    }
}
